## Objective

Implementing complex natural language queries in ExamonQL. ExamonQL is a dedicated query language for querying Examon, which is a holistsic monitoring framework employed at CINECA datacenters.

## Description

This project repository contains two Jupyter notebooks:

1. **Demo_ExamonQL.ipynb**: This notebook serves as a guideline, providing an overview of the implementation structure of queries in ExamonQL. It is essential to thoroughly review this file to become familiar with the historic telemetry data collected by Examon at CINECA data centers.

2. **examon_query.ipynb**: This notebook is the task at hand. You are required to complete the implementation of eight complex queries (in natural language), which are based on input from facility managers, engineers, and user-support at CINECA. The first four queries include partially filled codes and guidelines, with which you would be implementing them. Towards the end, the last four queries are challenging tasks where you are required to write the entire query yourself, utilizing the understanding gained from implementing the first four queries.

By engaging with this repository, users can:
- Gain proficiency in writing complex queries in ExamonQL.
- Understand how to extract valuable insights from performance data collected by Examon.
- Contribute to and collaborate on the development of ExamonQL query examples and best practices.

## Background - Examon

Examon is a holistic monitoring framework for High performance computing (HPC) systems. It is designed to collect data from various sources, including hardware sensors, software logs, and performance metrics, and store this data in a centralized repository.

![Examon](examon.png)

Examon’s data collection targets a diverse range of sources. The complexity of the collected data encompasses hardware sensors—such as CPU load across all cores, CPU clock, instructions per second, memory accesses, power consumption,fan speed, and ambient and component temperatures—along with workload-related information like job submissions and their characteristics. Additionally, Examon actively monitors compute node availability by capturing warning messages and alarms from diagnostic software tools used by system administrators. The figure further illustrates the granularity of Examon’s approach, showcasing separate plugins for each hardware component, each equipped with specific sensors. This design underscores Examon’s capacity to manage diverse data sources, contributing to its inherent capability to handle massive data complexity in monitoring. The open dataset available on [Zenodo](https://www.nature.com/articles/s41597-023-02174-3), covers a spectrum of metrics, from hardware parameters to system-related statistics. Examon employs a specific set of parameters and tags, and to interact with its dataset,it features a dedicated query language known as ExamonQL. This language allows users to access information stored in the database, including metadata, and generate dataframes of the queried results.

For an in-depth understanding of the data collected using Examon, visit the following link: [Nature Article](https://www.nature.com/articles/s41597-023-02174-3)

## Platform

You can work on your own personal PC's using any code editor (VSCODE or SublimeText etc), or work in the systems available in the Lab 7 at UNIBO. Or you may even just use Google Colab which offers a platform for running Jupyter notebooks. 

## Contact
- Prof.Andrea Bartolini (a.bartolini@unibo.it)
- Junaid Ahmed Khan - Tutor (junaidahmed.khan@unibo.it)

Get started by exploring the notebooks and engaging with the tasks. Happy querying!
